module.exports = [
	{
		name: 'alert',
		ignore: false,
		template: {
			html: './components/alert/alert.html',
			scss: './components/alert/_alert.scss'
		},
	},
	{
		name: 'spinner',
		ignore: false,
		template: {
			html: './components/spinner/spinner.html',
			scss: './components/spinner/_spinner.scss'
		},
	},
	{
		name: 'svg-icon',
		ignore: false,
		template: {
			html: './components/svg-icon/svg-icon.html',
			scss: './components/svg-icon/_svg-icon.scss'
		},
	},
	{
		name: 'map',
		ignore: false,
		template: {
			html: './components/map/map.html',
			scss: './components/map/_map.scss'
		},
	},
	{
		name: 'chart',
		ignore: false,
		template: {
			html: './components/chart/chart.html',
			scss: './components/chart/_chart.scss'
		},
	},

	{
		name: 'typography',
		ignore: false,
		template: {
			html: './components/typography/typography.html',
		},
	},
	{
		name: 'title',
		ignore: false,
		template: {
			scss: './components/title/_title.scss',
			html: './components/title/title.html',
		},
	},
	{
		name: 'text',
		ignore: false,
		template: {
			scss: './components/text/_text.scss',
			html: './components/text/text.html',
		},
	},
	{
		name: 'button',
		ignore: false,
		template: {
			html: './components/button/button.html',
			scss: './components/button/_button.scss'
		},
	},
	{
		name: 'link',
		ignore: false,
		template: {
			html: './components/link/link.html',
			scss: './components/link/_link.scss'
		},
	},
	{
		name: 'logo',
		ignore: false,
		template: {
			html: './components/logo/logo.html',
			scss: './components/logo/_logo.scss'
		},
	},
	{
		name: 'dropdown',
		ignore: false,
		template: {
			html: './components/dropdown/dropdown.html',
			scss: './components/dropdown/_dropdown.scss'
		},
	},
	{
		name: 'list',
		ignore: false,
		template: {
			html: './components/list/list.html',
			scss: './components/list/_list.scss'
		},
	},
	{
		name: 'nav',
		ignore: false,
		template: {
			html: './components/nav/nav.html',
			scss: './components/nav/_nav.scss'
		},
	},
	{
		name: 'form',
		ignore: false,
		template: {
			html: './components/form/form.html',
			scss: './components/form/_form.scss'
		},
	},
	{
		name: 'carousel',
		ignore: false,
		template: {
			html: './components/carousel/carousel.html',
			scss: './components/carousel/_carousel.scss',
			js: './components/carousel/carousel.js'
		},
	},
	{
		name: 'modal',
		ignore: false,
		template: {
			html: './components/modal/modal.html',
			scss: './components/modal/_modal.scss'
		},
	},
	{
		name: 'card',
		ignore: false,
		template: {
			html: './components/card/card.html',
			scss: './components/card/_card.scss'
		},
	},
	{
		name: 'pagination',
		ignore: false,
		template: {
			html: './components/pagination/pagination.html',
			scss: './components/pagination/_pagination.scss'
		},
	},
	{
		name: 'breadcrumbs',
		ignore: false,
		template: {
			html: './components/breadcrumbs/breadcrumbs.html',
			scss: './components/breadcrumbs/_breadcrumbs.scss'
		},
	},
	{
		name: 'collapse',
		ignore: false,
		template: {
			html: './components/collapse/collapse.html',
			scss: './components/collapse/_collapse.scss'
		},
	},
	{
		name: 'table',
		ignore: false,
		template: {
			html: './components/table/table.html',
			scss: './components/table/_table.scss'
		},
	},
	{
		name: 'tabs',
		ignore: false,
		template: {
			html: './components/tabs/tabs.html',
			scss: './components/tabs/_tabs.scss'
		},
	},
	{
		name: 'editor',
		ignore: false,
		template: {
			html: './components/editor/editor.html',
			scss: './components/editor/_editor.scss',
		},
	},
	{
		name: 'progressbar',
		ignore: false,
		template: {
			html: './components/progressbar/progressbar.html',
			scss: './components/progressbar/_progressbar.scss',
		},
	},
	{
		name: 'drag-and-drop',
		ignore: false,
		template: {
			html: './components/drag-and-drop/drag-and-drop.html',
			scss: './components/drag-and-drop/_drag-and-drop.scss',
		},
	},
]